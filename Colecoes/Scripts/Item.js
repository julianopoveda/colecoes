﻿$("#idColecaoPai").on("change", function () {
    $.ajax({
        url: urlpath,
        data: { id: this.value },
    success: function (data) {
        $("#idColecao").children("option").remove();
        for (var i = 0; i < data.lista.length; i++) {
            $("#idColecao").append('<option value=' + data.lista[i].Value + '>' + data.lista[i].Text + "</option>")
        }
    },
    error: function (data) {
        $("#idColecao").children("option").remove();
    }
});
});