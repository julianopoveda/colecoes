﻿using Colecoes.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Colecoes.Models
{
    public class Arquivo : arquivo, IOperacaoDB<arquivo>
    {
        private Entities db;


        public Arquivo()
        {
            db = new Entities();
        }

        #region Operações
        public bool Create(arquivo arquivo)
        {
            try
            {
                db.arquivo.Add(arquivo);
                db.SaveChanges();
                return true;//Se ocorreu tudo bem retorna true, senão vai estourar excecao
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Edit(arquivo arquivo)
        {
            try
            {
                db.Entry<arquivo>(arquivo).State = EntityState.Modified;
                db.SaveChanges();
                return true;//Se ocorreu tudo bem retorna true, senão vai estourar excecao
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                arquivo model = GetRegisterById(id);
                if (model == null)
                {
                    throw new Exception("Registro não localizado");
                }

                db.Entry(model).State = EntityState.Deleted;
                db.SaveChanges();
                return true;//Se ocorreu tudo bem retorna true, senão vai estourar excecao
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion


        public arquivo GetRegisterById(int id)
        {
            try
            {
                return db.arquivo.Find(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<arquivo> GetAllRegisters()
        {
            try
            {
                var arquivosList = db.arquivo.DefaultIfEmpty();
                return arquivosList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}