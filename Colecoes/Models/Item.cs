﻿using Colecoes.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Colecoes.Models
{
    public class Item : item, IOperacaoDB<item>
    {
        private Entities db;

        public Item()
        {
            db = new Entities();
        }

        public bool Create(item model)
        {
            try
            {
                db.item.Add(model);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Edit(item model)
        {
            try
            {
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                item model = GetRegisterById(id);
                if (model == null)
                {
                    throw new Exception("Registro não localizado");
                }

                db.Entry(model).State = EntityState.Deleted;
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public item GetRegisterById(int id)
        {
            try
            {
                return db.item.Find(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<item> GetAllRegisters()
        {
            try
            {
                var itemList = db.item.DefaultIfEmpty();
                return itemList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}