﻿using Colecoes.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;

namespace Colecoes.Models
{
    public class Colecao : colecao, IOperacaoDB<colecao>
    {
        private Entities db;//Não pode ser propriedade para não dar problema

        public Colecao()
        {
            db = new Entities();
        }

        public bool Create(colecao model)
        {
            try
            {
                db.colecao.Add(model);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Edit(colecao model)
        {
            try
            {
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                colecao model = GetRegisterById(id);
                if (model == null)
                {
                    throw new Exception("Registro não localizado");
                }
                db.Entry(model).State = EntityState.Deleted;
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public colecao GetRegisterById(int id)
        {
            try
            {
                return db.colecao.Find(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region Pesquisas/Filtros
        public IEnumerable<colecao> GetRegistersByParent(int idParent)
        {
            try
            {
                var colecaoList = db.colecao.Where(c => c.idColecaoPai == idParent).DefaultIfEmpty();
                return colecaoList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Recupera todos os registros que são pai de algué
        //TODO: Criar esta consulta
        public IEnumerable<colecao> GetRegistersParent()
        {
            try
            {
                //var colecaoList = db.colecao.Where(c => c.colecao1.Contains(c.idColecao)).DefaultIfEmpty();
                return null;//colecaoList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Obtém todas coleções cadastradas
        /// </summary>
        /// <returns>A lista com todos os registros</returns>
        public IEnumerable<colecao> GetAllRegisters()
        {
            try
            {
                var colecaoList = db.colecao.DefaultIfEmpty();
                return colecaoList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Obtém todas coleções cadastradas de um determinado tipo
        /// </summary>
        /// <param name="colecaoTipo">Informa qual o tipo da coleção</param>
        /// <returns>A lista com todos os registros</returns>
        public IEnumerable<colecao> GetAllRegisters(int colecaoTipo)
        {
            try
            {
                var colecaoList = db.colecao.Where(c=>c.idTipo == colecaoTipo).DefaultIfEmpty();
                return colecaoList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}