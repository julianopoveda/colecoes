﻿using Colecoes.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Colecoes.Models
{
    public class Tipo : tipo, IOperacaoDB<tipo>
    {
        private Entities db;

        public Tipo()
        {
            db = new Entities();
        }

        public bool Create(tipo model)
        {
            try
            {
                db.tipo.Add(model);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Edit(tipo model)
        {
            try
            {
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                tipo model = GetRegisterById(id);
                if (model == null)
                {
                    throw new Exception("Registro não localizado");
                }

                db.Entry(model).State = EntityState.Deleted;
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public tipo GetRegisterById(int id)
        {
            try
            {
                return db.tipo.Find(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Talvez este não seja necessário
        public IEnumerable<tipo> GetAllRegisters()
        {
            try
            {
                var tipoList = db.tipo.DefaultIfEmpty();
                return tipoList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}