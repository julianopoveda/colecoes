﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Resources;

namespace Colecoes.Operacoes
{
    public class CaptionGeneration
    {
        public bool WriteCaptionFile(Dictionary<string, string> captionList)
        {
            try
            {
                ResourceWriter objResourceWr = new ResourceWriter(MvcApplication.filename);
                foreach (var item in captionList)
                {
                    objResourceWr.AddResource(item.Key, item.Value);
                }

                objResourceWr.Close();
                return true;
            }
            catch (Exception) { throw; }
        }

        public Dictionary<string, string> ReadCaptionFile()
        {
            try
            {
                ResourceReader objResourceRd = new ResourceReader(MvcApplication.filename);
                Dictionary<string, string> CaptionDic = new Dictionary<string, string>();

                foreach (DictionaryEntry item in objResourceRd)
                {
                    CaptionDic.Add((string)item.Key, (string)item.Value);
                }
                objResourceRd.Close();
                return CaptionDic;
            }
            catch (Exception) { throw; }
        }

        /// <summary>
        /// Serve para criar o dicionário com os campos existentes nas models hoje
        /// </summary>
        public void CreateCaptionDictionary()
        {
            Dictionary<string, string> captionDictionary = new Dictionary<string, string>();
            //Tipo
            captionDictionary.Add("idTipo", "Tipo");
            captionDictionary.Add("TipoNome", "Nome do Tipo");

            //Coleção
            captionDictionary.Add("idColecao", "Coleção");
            captionDictionary.Add("idColecaoPai", "Coleção Pai");
            captionDictionary.Add("ColecaoNome", "Nome da Coleção");
            captionDictionary.Add("Descricao", "Descrição");

            //Item
            captionDictionary.Add("idItem", "Item");
            captionDictionary.Add("ItemNome", "Nome do Item");
            captionDictionary.Add("Codigo", "Código");
            captionDictionary.Add("ValorItem", "Valor do Item");

            //Arquivo
            captionDictionary.Add("idArquivo", "Arquivo");
            captionDictionary.Add("ArquivoCaminho", "Caminho do Arquivo");
            captionDictionary.Add("ArquivoNome", "Nome do Arquivo");

            WriteCaptionFile(captionDictionary);
        }
    }
}