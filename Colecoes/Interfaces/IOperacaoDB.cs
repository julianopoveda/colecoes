﻿using System.Collections.Generic;

namespace Colecoes.Interfaces
{
    /// <summary>
    /// Interface visa trabalhar com as operações comum a todas as models
    /// </summary>
    /// <typeparam name="T">O Tipo da model</typeparam>
    public interface IOperacaoDB<T>
    {
        bool Create(T model);
        bool Edit(T model);
        bool Delete(int id);

        T GetRegisterById(int id);
        IEnumerable<T> GetAllRegisters();
    }
}