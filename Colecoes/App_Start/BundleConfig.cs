﻿using System.Web.Optimization;

namespace Colecoes.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/scripts").Include(
                         "~/Scripts/jquery-{version}.min.js", "~/Scripts/bootstrap.min.js"));
            bundles.Add(new StyleBundle("~/content/css").Include(
                "~/Content/Site.css" , "~/Content/bootstrap.min.css"));

            BundleTable.EnableOptimizations = true;
        }
    }
}