﻿using Colecoes.App_Start;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Colecoes
{
    public class MvcApplication : System.Web.HttpApplication
    {
        public static string filename { get; private set; }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            filename = Server.MapPath("~/App_Data/Caption.resx");
            if (!System.IO.File.Exists(filename))
            {
                Colecoes.Operacoes.CaptionGeneration objCaptionGeneration = new Operacoes.CaptionGeneration();
                objCaptionGeneration.CreateCaptionDictionary();
            }
            
        }
    }
}
