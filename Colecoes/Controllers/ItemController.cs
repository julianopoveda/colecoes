﻿using Colecoes.Models;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Colecoes.Controllers
{
    public class ItemController : Controller
    {
        private Entities db = new Entities();

        // GET: Item
        public ActionResult Index()
        {
            return View(db.item.ToList());
        }

        // GET: Item/Details/5
        public ActionResult Details(int id)
        {
            Item objItem = new Item();
            item model = objItem.GetRegisterById(id);

            if (model == null)
            {
                return HttpNotFound();
            }

            return View(model);
        }

        // GET: Item/Create
        public ActionResult Create()
        {
            PreencherDropDownList();
            Item model = new Item();

            return View(model);
        }

        public ActionResult CreateForParent()
        {
            PreencherDropDownList();
            Item model = new Item();

            return View("Create", model);
        }

        // POST: Item/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(item model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Item objItem = new Item();
                    objItem.Create(model);

                    return RedirectToAction("Index");
                }
                else
                {
                    PreencherDropDownList(model.idColecao);
                    return View(model);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                PreencherDropDownList(model.idColecao);
                return View(model);
            }
        }

        // GET: Item/Edit/5
        public ActionResult Edit(int id)
        {
            Item objItem = new Item();
            item model = objItem.GetRegisterById(id);

            if (model == null)
            {
                return HttpNotFound();
            }

            PreencherDropDownList(model.colecao.idColecaoPai, model.idColecao);
            return View(model);
        }

        public ActionResult EditForParent(int id)
        {
            Item objItem = new Item();
            item model = objItem.GetRegisterById(id);

            if (model == null)
            {
                return HttpNotFound();
            }

            PreencherDropDownList(model.idColecao);//Nesse caso não quero saber se tem filho. Estou editando diretamente
            return View("Edit", model);
        }

        // POST: Item/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(item model)
        {
            Item objItem = new Item();
            try
            {
                if (ModelState.IsValid)
                {
                    objItem.Edit(model);

                    return RedirectToAction("Index");
                }
                else
                {
                    PreencherDropDownList(model.idColecao);
                    return View(model);
                }
            }
            catch (Exception ex)
            {
                //Aqui recupero o registro original, para ver se ele não foi alterado ou excluido
                item modelRec = objItem.GetRegisterById(model.idItem);

                if (modelRec == null)
                {
                    ModelState.AddModelError("", "Registro Excluído");
                }
                else
                {
                    ModelState.AddModelError("", ex.Message);
                    model = modelRec;
                }

                PreencherDropDownList(model.idColecao);
                return View(model);
            }
        }

        // GET: Item/Delete/5
        public ActionResult Delete(int id)
        {
            Item objItem = new Item();
            item itemReg = objItem.GetRegisterById(id);

            if (itemReg == null)
            {
                return HttpNotFound();
            }
            return View(itemReg);
        }

        // POST: /Tipo/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Item objItem = new Item();
            try
            {
                objItem.Delete(id);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                item model = objItem.GetRegisterById(id);
                ModelState.AddModelError("", ex.Message);
                return View(model);
            }
        }

        #region Operacoes
        [HttpGet]
        public ActionResult LancarLoteItem()
        {
            PreencherDropDownList();
            return View(new item());
        }

        [HttpPost]
        public ActionResult LancarLoteItem(item model, int repeticoes)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Item objItem = new Item();
                    int codigoInicial = 0;
                    string codigoStr = string.Empty;
                    //Tenta converter para inteiro o código do item, se não conseguir significa que o código é uma string não numeral.
                    //Então atribui o valor do cógido a string ao codigoStr
                    if (!int.TryParse(model.Codigo, out codigoInicial))
                    {
                        codigoStr = model.Codigo;
                    }

                    for (int i = 0; i < repeticoes; i++)
                    {
                        model.Codigo = codigoStr + codigoInicial.ToString();
                        objItem.Create(model);
                        codigoInicial++;
                    }
                }
                else
                {
                    PreencherDropDownList(model.idColecao);
                    return View(model);
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                PreencherDropDownList(model.idColecao);
                ModelState.AddModelError("", ex.Message);
                return View(model);
            }
        }
        #endregion

        #region Funções Suporte
        public  JsonResult GetFilhos(int id)
        {
            Colecao objColecao = new Colecao();
            SelectList selectList =  new SelectList(objColecao.GetRegistersByParent(id), "idColecao", "ColecaoNome");

            return Json(new { lista = selectList }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Preencher as ddl's de tipo e colecao pai
        /// </summary>
        /// <param name="idColecaoPai">Valor que deve vir selecionado do ddl colecao(opcional)</param>
        private void PreencherDropDownList(int? idColecaoPai = null, int? idColecao = null)
        {
            ViewBag.ColecaoPai = new SelectList(db.colecao, "idColecao", "ColecaoNome", idColecaoPai);
            ViewBag.Colecao = new SelectList(db.colecao.Where(c => c.idColecaoPai == idColecaoPai), "idColecao", "ColecaoNome", idColecao);
            //ViewBag.Tipo = new SelectList(db.tipo, "idTipo", "TipoNome", idColecaoPai);
            //ViewBag.idImagemCapa = new SelectList(db.arquivo, "idArquivo", "ArquivoCaminho", colecao.idImagemCapa);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        #endregion
    }
}
