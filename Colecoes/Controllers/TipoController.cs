﻿using Colecoes.Models;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Colecoes.Controllers
{
    public class TipoController : Controller
    {
        private Entities db = new Entities();

        // GET: /Tipo/
        public ActionResult Index()
        {
            return View(db.tipo.ToList());
        }

        // GET: /Tipo/Details/5
        public ActionResult Details(int id)
        {
            Tipo objTipo = new Tipo();
            tipo model = objTipo.GetRegisterById(id);

            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        // GET: /Tipo/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /Tipo/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(tipo model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    Tipo objTipo = new Tipo();
                    objTipo.Create(model);

                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                    return View(model);
                }
            }

            return View(model);
        }

        // GET: /Tipo/Edit/5
        public ActionResult Edit(int id)
        {
            Tipo objTipo = new Tipo();
            tipo tipo = objTipo.GetRegisterById(id);

            if (tipo == null)
            {
                return HttpNotFound();
            }
            return View(tipo);
        }

        // POST: /Tipo/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(tipo model)
        {
            Tipo objTipo = new Tipo(); try
            {
                if (ModelState.IsValid)
                {
                    objTipo.Edit(model);

                    return RedirectToAction("Index");
                }
                else
                {
                    return View(model);
                }
            }
            catch (Exception ex)
            {
                //Aqui recupero o registro original, para ver se ele não foi alterado ou excluido
                tipo modelRec = objTipo.GetRegisterById(model.idTipo);

                if (modelRec == null)
                {
                    ModelState.AddModelError("", "Registro Excluído");
                }
                else
                {
                    ModelState.AddModelError("", ex.Message);
                    model = modelRec;
                }

                return View(model);
            }

        }

        // GET: /Tipo/Delete/5
        public ActionResult Delete(int id)
        {
            Tipo objTipo = new Tipo();
            tipo tipo = objTipo.GetRegisterById(id);

            if (tipo == null)
            {
                return HttpNotFound();
            }
            return View(tipo);
        }

        // POST: /Tipo/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Tipo objTipo = new Tipo();
            try
            {
                objTipo.Delete(id);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                tipo model = objTipo.GetRegisterById(id);
                ModelState.AddModelError("", ex.Message);
                return View(model);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
