﻿using Colecoes.Models;
using Colecoes.Operacoes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Mvc;

namespace Colecoes.Controllers
{
    public class OperacoesController : Controller
    {
        private Entities db = new Entities();

        // GET: Operacoes
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult CreateTree()
        {
            ViewBag.Tipo = new SelectList(db.tipo, "idTipo", "TipoNome");
            return View();
        }

        [HttpPost]
        public ActionResult CreateTree(string initialPath, int idTipo, string fileExtentions, bool includeRoot)//o initial pathy deve ser a pasta raiz
        {
            try
            {
                int? idColecao = null;
                //Incluir os arquivos da raiz
                if (includeRoot)
                {
                    idColecao = CreateColecao(idTipo, Path.GetFileName(initialPath));
                    VarrerArquivos(idColecao.Value, initialPath, fileExtentions);
                }
                VarrerDiretorios(initialPath, idTipo, fileExtentions, idColecao);//Ainda não tá funcionando com os arquivos da pasta raiz que vem daqui
                return RedirectToAction("Index", "Colecao");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                //repopular a model
                object model = new { initialPath = initialPath, idTipo = idTipo, fileExtentions = fileExtentions };
                return View(model);
            }
        }

        public ActionResult CadastrarNomeCamposExibição()
        {
            return View();
        }

        public ActionResult AlterarNomeCamposExibicao()
        {
            CaptionGeneration objCaptionGeneration = new CaptionGeneration();
            return View(objCaptionGeneration.ReadCaptionFile());
        }

        [HttpPost]
        public ActionResult AlterarNomeCamposExibicao(string[] key, string[] value)
        {
            try
            {
                Dictionary<string, string> captionDictionary = new Dictionary<string, string>();
                for (int i = 0; i < key.Length; i++)
                {
                    captionDictionary.Add(key[i], value[i]);
                }
                return View(captionDictionary);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                //repopular a model

                Dictionary<string, string> captionDictionary = new Dictionary<string, string>();
                for (int i = 0; i < key.Length; i++)
                    captionDictionary.Add(key[i], value[i]);

                return View(captionDictionary);
            }
        }


        private void VarrerDiretorios(string path, int idTipo, string fileExtentions, int? idColecaoPai = null)
        {
            string[] directories = Directory.GetDirectories(path);
            for (int i = 0; i < directories.Length; i++)
            {
                string fullPath = Path.GetFullPath(directories[i]);
                //primeiro crio a coleção
                int idColecao = CreateColecao(idTipo, Path.GetFileName(directories[i]), idColecaoPai);
                //Agora lanço todos os itens(arquivos do diretório)
                VarrerArquivos(idColecao, fullPath, fileExtentions);

                //Após Lançar todos os arquivos dentro de um diretório, verifico se ele possui diretórios Filhos passando ele como parâmentro
                VarrerDiretorios(fullPath, idTipo, fileExtentions, idColecao);
            }

        }

        private bool VarrerArquivos(int idColecao, string fullPath, string fileExtentions)
        {
            //Quebro a lista de extensões e faço a varredura dos arquivos buscando pelo arquivo em questão.
            //Verifico se o string passado não é nulo, se for busco tudo.
            string[] extensions = string.IsNullOrEmpty(fileExtentions) ? new string[] { "*" } : fileExtentions.Split(',');

            for (int i = 0; i < extensions.Length; i++)
            {
                string extension = string.Format("*.{0}", extensions[i]);

                string[] files = Directory.GetFiles(fullPath, extension);

                foreach (var arquivo in files)
                {
                    CreateItem(idColecao, Path.GetFileNameWithoutExtension(arquivo));
                }
            }
            return true;
        }

        private int CreateColecao(int idTipo, string colecaoNome, int? idColecaoPai = null)
        {
            Colecao objColecao = new Colecao();
            colecao colecao = new colecao();

            colecao.idColecaoPai = idColecaoPai;
            colecao.idTipo = idTipo;
            colecao.ColecaoNome = colecaoNome;
            objColecao.Create(colecao);

            return colecao.idColecao;
        }

        private bool CreateItem(int idColecao, string fileName)
        {
            Item objItem = new Item();
            item model = new item();

            model.ItemNome = fileName;
            model.idColecao = idColecao;
            objItem.Create(model);

            return true;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}