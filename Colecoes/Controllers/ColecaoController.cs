﻿using Colecoes.Models;
using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;

namespace Colecoes.Controllers
{
    public class ColecaoController : Controller
    {
        private Entities db = new Entities();

        // GET: /Colecao/
        public ActionResult Index()
        {
            var colecao = db.colecao.Include(c => c.arquivo).Include(c => c.colecao2).Include(c => c.tipo);
            return View(colecao.ToList());
        }

        // GET: /Colecao/Details/5
        public ActionResult Details(int id)
        {
            Colecao objColecao = new Colecao();

            colecao model = objColecao.GetRegisterById(id);
            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        // GET: /Colecao/Create
        public ActionResult Create()
        {
            PreencherDropDownList();
            Colecao model = new Colecao();
            return View(model);
        }

        // POST: /Colecao/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //Função serve para cadastrar tanto coleções pai quanto coleções filhas
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(colecao model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Colecao objColecao = new Colecao();
                    objColecao.Create(model);
                    return RedirectToAction("Index");
                }
                else
                {
                    PreencherDropDownList(model.idTipo, model.idColecaoPai);
                    return View(model);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                PreencherDropDownList(model.idTipo, model.idColecaoPai);
                return View(model);
            }
        }

        // GET: /Colecao/Edit/5
        public ActionResult Edit(int id)
        {
            Colecao objColecao = new Colecao();
            colecao model = objColecao.GetRegisterById(id);

            if (model == null)
            {
                return HttpNotFound();
            }
            PreencherDropDownList(model.idTipo, model.idColecaoPai);

            return View(model);
        }

        // POST: /Colecao/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(colecao model)
        {
            Colecao objColecao = new Colecao();
            try
            {
                if (ModelState.IsValid)
                {
                    objColecao.Edit(model);

                    return RedirectToAction("Index");
                }
                else
                {
                    PreencherDropDownList(model.idTipo, model.idColecaoPai);
                    return View(model);
                }
            }
            catch (Exception ex)
            {
                colecao modelRec = objColecao.GetRegisterById(model.idColecao);

                if (modelRec == null)
                {
                    ModelState.AddModelError("", "Registro Excluído");
                }
                else
                {
                    ModelState.AddModelError("", ex.Message);
                    model = modelRec;
                }

                PreencherDropDownList(model.idTipo, model.idColecaoPai);
                return View(model);
            }
        }

        // GET: /Colecao/Delete/5
        public ActionResult Delete(int id)
        {
            Colecao objColecao = new Colecao();
            colecao colecao = objColecao.GetRegisterById(id);
            if (colecao == null)
            {
                return HttpNotFound();
            }
            return View(colecao);
        }

        // POST: /Colecao/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Colecao objColecao = new Colecao();
            try
            {
                objColecao.Delete(id);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                colecao model = objColecao.GetRegisterById(id);
                ModelState.AddModelError("", ex.Message);
                return View(model);
            }
        }

        #region Funções Suporte
        /// <summary>
        /// Preencher as ddl's de tipo e colecao pai
        /// </summary>
        /// <param name="idColecaoPai">Valor que deve vir selecionado do ddl colecaoPai(opcional)</param>
        /// <param name="idTipo">Valor que deve vir selecionado do ddl tipo(opcional)</param>
        private void PreencherDropDownList(int? idTipo = null, int? idColecaoPai = null)
        {
            ViewBag.ColecaoPai = new SelectList(db.colecao, "idColecao", "ColecaoNome", idColecaoPai);
            ViewBag.Tipo = new SelectList(db.tipo, "idTipo", "TipoNome", idTipo);
            //ViewBag.idImagemCapa = new SelectList(db.arquivo, "idArquivo", "ArquivoCaminho", colecao.idImagemCapa);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        #endregion
    }
}
