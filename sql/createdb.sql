create database colecoes

use colecoes

create table tipo(
	idTipo int primary key identity,
	TipoNome varchar(255),
)

create table arquivo(
	idArquivo int primary key identity,
	ArquivoCaminho varchar(4000) not null,
	ArquivoNome varchar(255) not null
)

create table colecao(
	idColecao int primary key identity,
	idColecaoPai int,
	idTipo int,
	idImagemCapa int,
	ColecaoNome varchar(255) not null,
	Descricao varchar(255),

	foreign key (idColecaoPai) references colecao(idColecao),
	foreign key (idTipo) references tipo(idTipo),
	foreign key (idImagemCapa) references arquivo(idArquivo)
)

create table item(
	idItem int primary key identity,
	idColecao int not null,
	idLocalArquivo int,	
	ItemNome varchar(255) not null,
	Codigo varchar(255),
	Descricao varchar(255),
	ValorItem decimal(10,2),

	foreign key (idColecao) references colecao(idColecao),	
	foreign key (idLocalArquivo) references arquivo(idArquivo)
)